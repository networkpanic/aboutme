FROM hugomods/hugo:ci AS hugo

COPY . /src

RUN hugo mod tidy && npm i
RUN hugo --minify --gc --enableGitInfo

FROM nginx
COPY --from=hugo /src/public /usr/share/nginx/html
